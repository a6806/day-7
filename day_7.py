import argparse
from typing import TextIO


def compute_fuel_usage_1(target, crabs):
    fuel = 0
    for crab in crabs:
        fuel += abs(target - crab)
    return fuel


def compute_fuel_usage_2(target, crabs):
    fuel = 0
    for crab in crabs:
        distance = abs(target - crab)
        fuel += int((distance ** 2 + distance) / 2)
    return fuel


def part_1(input_file: TextIO):
    crabs = [int(x) for x in input_file.read().split(',')]
    possibilities = [compute_fuel_usage_1(target, crabs) for target in range(min(crabs), max(crabs))]
    return min(possibilities)


def part_2(input_file: TextIO):
    crabs = [int(x) for x in input_file.read().split(',')]
    possibilities = [compute_fuel_usage_2(target, crabs) for target in range(min(crabs), max(crabs))]
    return min(possibilities)


def main():
    parser = argparse.ArgumentParser(description='Advent of Code Day 7')
    part_group = parser.add_mutually_exclusive_group(required=True)
    part_group.add_argument('--part-1', action='store_true', help='Run Part 1')
    part_group.add_argument('--part-2', action='store_true', help='Run Part 2')
    parser.add_argument('--example', action='store_true', help='Run part with example data')
    args = parser.parse_args()

    part_number = '1' if args.part_1 else '2'
    function = part_1 if args.part_1 else part_2
    example = '_example' if args.example else ''
    input_file_path = f'input/part_{part_number}{example}.txt'

    with open(input_file_path, 'r') as input_file:
        print(function(input_file))


if __name__ == '__main__':
    main()
